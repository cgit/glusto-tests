#  Copyright (C) 2021 Red Hat, Inc. <http://www.redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along`
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from glusto.core import Glusto as g
from glustolibs.gluster.gluster_base_class import GlusterBaseClass, runs_on
from glustolibs.gluster.exceptions import ExecutionError
from glustolibs.gluster.volume_libs import shrink_volume
from glustolibs.io.utils import collect_mounts_arequal


@runs_on([['distributed'], ['glusterfs']])
class TestRebalanceMultipleShrinks(GlusterBaseClass):

    def setUp(self):

        self.get_super_method(self, 'setUp')()

        # Changing dist_count to 6
        self.volume['voltype']['dist_count'] = 6

        # Setup Volume
        if not self.setup_volume_and_mount_volume([self.mounts[0]]):
            raise ExecutionError("Failed to Setup and mount volume")

        self.first_client = self.mounts[0].client_system

    def tearDown(self):

        # Unmount and clean volume
        if not self.unmount_volume_and_cleanup_volume([self.mounts[0]]):
            raise ExecutionError("Failed to Cleanup Volume")

        # Calling GlusterBaseClass tearDown
        self.get_super_method(self, 'tearDown')()

    def test_rebalance_multiple_shrinks(self):
        """
        Test case:
        1. Modify the distribution count of a volume
        2. Create a volume, start it and mount it
        3. Create some file on mountpoint
        4. Collect arequal checksum on mount point pre-rebalance
        5. Do the following 3 times:
        6. Shrink the volume
        7. Collect arequal checksum on mount point post-rebalance
           and compare with value from step 4
        """

        # Create some file on mountpoint
        cmd = ("cd %s; for i in {1..500} ; do "
               "dd if=/dev/urandom of=file$i bs=10M count=1; done"
               % self.mounts[0].mountpoint)
        ret, _, _ = g.run(self.first_client, cmd)
        self.assertEqual(ret, 0, "IO failed on volume %s"
                         % self.volname)

        # Collect arequal checksum before rebalance
        arequal_checksum_before = collect_mounts_arequal(self.mounts[0])

        for _ in range(3):
            # Shrink volume
            ret = shrink_volume(self.mnode, self.volname,
                                rebalance_timeout=16000)
            self.assertTrue(ret, "Failed to remove-brick from volume")
            g.log.info("Remove-brick rebalance successful")

            # Collect arequal checksum after rebalance
            arequal_checksum_after = collect_mounts_arequal(self.mounts[0])

            # Check for data loss by comparing arequal before and after
            # rebalance
            self.assertEqual(arequal_checksum_before, arequal_checksum_after,
                             "arequal checksum is NOT MATCHNG")
            g.log.info("arequal checksum is SAME")
